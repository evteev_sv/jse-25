package com.nlmk.evteev;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListMarker {

    private static final String fileName = "serialise.csv";
    private static final String headerString = "FirstName;LastName;BirthDate;E-mail;\n";
    Logger logger = Logger.getLogger(ListMarker.class.getName());

    public void makeList(List<Person> personList) throws IOException {
        if (personList.isEmpty()) {
            logger.log(Level.ALL, "Объект для просмотра не существует!");
            return;
        }
        List<String[]> strings = new ArrayList<>();
        Class aClass = personList.get(0).getClass();
        for (Person person : personList) {
            if (!aClass.equals(person.getClass())) {
                throw new IllegalArgumentException("Типы классов не совпадают!");
            }
            Class clazz = person.getClass();
            do {
                parseClass(clazz.getDeclaredFields(), strings, person);
                clazz = clazz.getSuperclass();
            } while (clazz.getSuperclass() != null);
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(headerString);
        strings.forEach(strings1 -> {
            for (int i = 0; i < strings1.length; i++) {
                stringBuilder.append(strings1[i] + ";");
            }
            stringBuilder.append("\n");
        });
        serializeToFile(stringBuilder.toString());
    }

    private void parseClass(Field[] fields, List<String[]> strings, Person person) {
        String[] str = new String[fields.length];
        int i = 0;
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                String data = field.get(person) == null ? "" : field.get(person).toString();
                str[i] = data;
                i++;
            } catch (IllegalAccessException e) {
                logger.log(Level.ALL, e.getMessage());
            }
        }
        strings.add(str);
    }

    private void serializeToFile(String data) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write(data);
        writer.close();
    }
}
