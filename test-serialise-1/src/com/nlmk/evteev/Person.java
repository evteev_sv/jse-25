package com.nlmk.evteev;

import java.io.Serializable;
import java.time.LocalDate;

public class Person implements Serializable {

    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String email;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(String firstName, String lastName, LocalDate birthDate, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.email = email;
    }
}
