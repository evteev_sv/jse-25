package com.nlmk.evteev;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        List<Person> personLiist = new ArrayList<>();
        Person person = new Person("Igor", "Petrov");
        Person person1 = new Person("Ivan", "Petrov",
                LocalDate.parse("25.05.1965", DateTimeFormatter.ofPattern("dd.MM.yyyy")), "test@mail.com");
        personLiist.add(person);
        personLiist.add(person1);
        ListMarker listMarker = new ListMarker();
        try {
            listMarker.makeList(personLiist);
        } catch (IOException e) {
            logger.log(Level.ALL, "Ошибка сериализации! \n".concat(e.getMessage()));
        }
    }
}
